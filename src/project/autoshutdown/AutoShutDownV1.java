package project.autoshutdown;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import project.autoshutdown.DigitalTimeUI.DigitalClock;

public class AutoShutDownV1 {
	
	protected static JFrame window;
	JPanel allPanel,coutDownP,timeCurP,setP,modeP,buttonP;
	JPanel mode1P,mode2P,mode3P,coutDown1P ,coutDown2P; 
	JLabel actionL,shutL,timeCurL, coutDownL,coutDown2L,hrL,minL;
	static JTextField hrTxt;
	static JTextField minTxt;
	JButton stopB,startB,exitB;
	static JRadioButton shutDown, reStart, logOff, sleep, hibernate;
	ButtonGroup butG;
	     
	    public void setUpLabelTextField(JPanel panel, JLabel label, JTextField txt) {
	        panel.add(label);
	        panel.add(txt);
	        txt.setHorizontalAlignment(JTextField.RIGHT);
	    }
	    public void createComponents() {
	        window = new JFrame("Auto ShutDown");
	         
	        allPanel = new JPanel(new BorderLayout());//panel หลัก ประกอบด้วย >>coutDownP, timeCurP,setP
	        allPanel.setOpaque(false);
	 	   coutDownP = new JPanel(new BorderLayout());
	 	   coutDownP.setOpaque(false);
	 	   timeCurP = new JPanel();
	 	   timeCurP.setOpaque(false);
	 	   setP = new JPanel(new BorderLayout());//ประกอบด้วย>>modeP, buttonP
	 	   setP.setOpaque(false);
	 	  	 	
	 	  
	 	   modeP = new JPanel(new BorderLayout());//ประกอบด้วย>> mode1P,mode2P,mode3P
	 	  modeP.setOpaque(false);
	 	   mode1P = new JPanel();
	 	  mode1P.setOpaque(false);
	 	   mode2P = new JPanel();
	 	  mode2P.setOpaque(false);
	 	 mode3P = new JPanel();
	 	 mode3P.setOpaque(false);
	 	   buttonP = new JPanel(new FlowLayout());
	 	  buttonP.setOpaque(false);
	 	   
	 	   allPanel.add(timeCurP,BorderLayout.NORTH);
	 	   allPanel.add(coutDownP,BorderLayout.CENTER);	 	  
	 	   allPanel.add(setP,BorderLayout.SOUTH);
	 	  //allPanel.setLayout(null);
	 	   
	 	   setP.add(modeP,BorderLayout.NORTH);
	 	   setP.add( buttonP,BorderLayout.SOUTH);
	        
	 	 //stopB = new JButton("STOP");
	 	 //  stopB.setPreferredSize(new Dimension(80, 30));
	 	  ImageIcon start = new ImageIcon("bin/images/start.jpg");
	 	    ImageIcon start2 = new ImageIcon("bin/images/start2.jpg");
	 	    ImageIcon start3 = new ImageIcon("bin/images/start3.jpg");
	        startB = new JButton(start);
	        startB.setOpaque(false);
	        startB.setContentAreaFilled(false);
	        startB.setBorderPainted(false);
	        startB.setRolloverIcon(start2);
	        startB.setPressedIcon(start3);
	        startB.setFocusPainted(false);
	        ImageIcon exit = new ImageIcon("bin/images/exit.jpg");
	        ImageIcon exit2 = new ImageIcon("bin/images/exit2.jpg");
	        ImageIcon exit3 = new ImageIcon("bin/images/exit3.jpg");
	        exitB = new JButton(exit);	
	        exitB.setOpaque(false);
	        exitB.setContentAreaFilled(false);
	        exitB.setBorderPainted(false);
	        exitB.setRolloverIcon(exit2);
	        exitB.setPressedIcon(exit3);
	        exitB.setFocusPainted(false);
	        
	        shutDown = new JRadioButton("ShutDown",true);
	        shutDown.setOpaque(false);
	        reStart = new JRadioButton("Restart");
	        reStart.setOpaque(false);
	        logOff = new JRadioButton("LogOff");
	        logOff.setOpaque(false);
	        sleep = new JRadioButton("Sleep");
	        sleep.setOpaque(false);
	        hibernate = new JRadioButton("Hibernate");
	        hibernate.setOpaque(false);
	        butG = new ButtonGroup();
	        butG.add(shutDown);
	        butG.add(reStart);
	        butG.add( logOff);
	        butG.add(sleep);
	        butG.add(hibernate);	 	  
	        
	        shutL = new JLabel("Shutdown At :");
	        actionL = new JLabel("Action :");
	        timeCurL = new JLabel("Time Current :");
	        coutDownL = new JLabel("Time  remaining :");
	        hrL  = new JLabel("hours");
	        minL = new JLabel("minutes");
	        Font fn = new Font("Courier New",Font.BOLD,60);
	        coutDown2L = new JLabel("HH:MM:SS");
	        coutDown2L.setFont(fn);

	        hrTxt = new JTextField("0",5);
	        minTxt = new JTextField("0",5);

	        //add ลง PanelMode
	        modeP.add(mode1P,BorderLayout.NORTH);
	        modeP.add(mode2P,BorderLayout.CENTER);
	        modeP.add(mode3P,BorderLayout.SOUTH);
	        mode1P.add(shutL);
	        mode1P.add(hrTxt);
	        mode1P.add(hrL);
	        mode1P.add(minTxt);
	        mode1P.add(minL);
	        mode2P.add(actionL);
	        mode2P.add(shutDown);
	        mode2P.add(reStart);
	        mode2P.add(logOff);
	        mode3P.add(sleep);
	        mode3P.add(hibernate);
	        //panel TimeCurrent
	        DigitalClock myClock = new DigitalClock();//นาฬิกา	   
	        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	        myClock.setOpaque(false);
	        JLabel l = new JLabel(date);
	       timeCurP.add(timeCurL);
	        timeCurP.add(l);
	        timeCurP.add(myClock);
	        //panel remaining
	        coutDownP.add(coutDownL,BorderLayout.NORTH);  
	        
	 	   //add ลง panelButton
	        buttonP.add(startB);
	 	  buttonP.add(exitB);
	 	  window.setContentPane(allPanel);
	    }
	     
	    public void setFrameFeatures() {
	    	//ImagePanel panel = new ImagePanel(new ImageIcon("bin/images/background.jpg").getImage());
	    	ImageIcon icon = new ImageIcon("bin/images/icon.jpg");
	    	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	        window.setVisible(true);
	        window.setSize(350,350);	        
	        window.setIconImage(icon.getImage());
	        window.setBackground(Color.lightGray);
	        //display the window at the middle of the computer screen
	        int w = window.getSize().width;
	        int h = window.getSize().height;
	        int x = (dim.width - w)/2;
	        int y = (dim.height - h)/2;
	        window.setLocation(x,y);
	 
	        window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	        
	    }
	    public static void createAndShowGUI() {
	    	AutoShutDownV1 asd = new AutoShutDownV1();
	    	asd.createComponents();
	    	asd.setFrameFeatures();
	    }
	     
	    public static void main(String[] args) {
	        SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	                
	            }
	        });
	    }
}
//พื้นหลัง
class ImagePanel extends JPanel {

	  private Image img;

	  public ImagePanel(String img) {
	    this(new ImageIcon(img).getImage());
	  }

	  public ImagePanel(Image img) {
	    this.img = img;
	    Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
	    setPreferredSize(size);
	    setMinimumSize(size);
	    setMaximumSize(size);
	    setSize(size);
	    setLayout(null);
	  }

	  public void paintComponent(Graphics g) {
	    g.drawImage(img, 0, 0, null);
	  }

	}

