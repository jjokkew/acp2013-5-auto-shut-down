package project.autoshutdown;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import project.autoshutdown.DigitalTimeUI.DigitalClock;

public class AutoShutDown extends JFrame implements ActionListener,Runnable{
   
	private static final long serialVersionUID = 1L;
	
	public boolean running;
	public static Object MUTEX;
	public int sec;
	public int count;
	Thread t;
	JFrame frame;
	JPanel allPanel,coutDownP,timeCurP,setP,modeP,buttonP;
	JPanel mode1P,mode2P,mode3P; 
	static String format = "dd /MM /yyyy";
	JLabel actionL,shutL,timeCurL, coutDownL,hrL,minL;
	JTextField hrTxt,minTxt;
	JButton cancelB,okB;
	JRadioButton shutDown,reStart,logOff,sleep,hibernate;
	ButtonGroup butG;
	
 public AutoShutDown() {
       createComponent();
       
 }
   public final Component createComponent(){
	   
	   //panel
	   allPanel = new JPanel(new BorderLayout());//panel หลัก ประกอบด้วย >>coutDownP, timeCurP,setP
	   coutDownP = new JPanel();
	   timeCurP = new JPanel();
	   setP = new JPanel(new GridLayout(2,1));//ประกอบด้วย>>modeP, buttonP
	  
	   modeP = new JPanel(new GridLayout(2,1));//ประกอบด้วย>> mode2P,mode3P
	   mode1P = new JPanel();
	   mode2P = new JPanel();
	   mode3P = new JPanel();
	   buttonP = new JPanel(new FlowLayout());
	   
	   allPanel.add(coutDownP,BorderLayout.NORTH);
	   allPanel.add(timeCurP,BorderLayout.CENTER);
	   allPanel.add(setP,BorderLayout.SOUTH);
	   
	   setP.add(modeP);
	   setP.add( buttonP);
       
       cancelB = new JButton("Cancel");
       okB = new JButton("OK");
       //addListener
       okB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (sleep.isSelected()) {
					System.out.print("System is sleeping ");
			        //System.out.println(timeRemain());
			        timeRemain();			   			       
				}
			}
		});
       
       shutDown = new JRadioButton("ShutDown",true);
       reStart = new JRadioButton("Restart");
       logOff = new JRadioButton("LogOff");
       sleep = new JRadioButton("Sleep");
       hibernate = new JRadioButton("Hibernate");
       butG = new ButtonGroup();
       butG.add(shutDown);
       butG.add(reStart);
       butG.add( logOff);
       butG.add(sleep);
       butG.add(hibernate);
			
       //ActionCommand
       //cancelB.setActionCommand("cancel");
       
       shutL = new JLabel("Shutdown At :");
       actionL = new JLabel("Action :");
       timeCurL = new JLabel("Time Current :");
       coutDownL = new JLabel("Time  remaining :");
       hrL  = new JLabel("hours");
       minL = new JLabel("minutes");

       hrTxt = new JTextField(5);
       minTxt = new JTextField(5);

       //add ลง PanelMode
       modeP.add(mode2P);
       modeP.add(mode3P);
       mode2P.add(shutL);
       mode2P.add(hrTxt);
       mode2P.add(hrL);
       mode2P.add(minTxt);
       mode2P.add(minL);
       mode3P.add(actionL);
       mode3P.add(shutDown);
       mode3P.add(reStart);
       mode3P.add(logOff);
       mode3P.add(sleep);
       mode3P.add(hibernate);
       //panel TimeCurrent
       DigitalClock myClock = new DigitalClock();//นาฬิกา
       timeCurP.add( timeCurL);
       timeCurP.add(myClock);
       //panel remaining
       coutDownP.add(coutDownL);
	   //add ลง panelButton
       buttonP.add(okB);
	   buttonP.add(cancelB);
       
       frame = new JFrame("AutoShutDown");
       frame.add(allPanel);
       setFrameFeatures(frame);
      return frame;
   }
  
  public  void setFrameFeatures(JFrame window) {
	  window.setLocation(400,200);
	  window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  window.setSize(500,400);
	  window.setVisible(true); 
  
}
public int timeRemain(){
	int c = Integer.parseInt(hrTxt.getText());
	int a = Integer.parseInt(minTxt.getText());
	int b = (c*3600)+(a* 60);
	for(sec = b ;sec>=0;sec--){
           System.out.print(" in " + sec + " second ");
           //synchronized(syncMutex.mutex){
               syncMutex.val = sec;
               count = syncMutex.val;
               frame.setTitle("Remain " + count + " Second To Shut Down");
               
          // }
           try {       
           Thread.sleep(1000);
       } catch (InterruptedException ex) {
           Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
       if(count == 0){
    	   frame.setTitle("System Is Shutting Down....");
       }
     return sec;
   }

//ควบคุมโปรแกรม
   public void shutDown(){
	   String shutdownCmd = "shutdown -s";
           try {
        	   Runtime.getRuntime().exec(shutdownCmd);
           } catch (IOException ex) {
               Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
           }
   }
   
   public void restart(){
	   String shutdownCmd = "shutdown -r";
	           try {
	        	   Runtime.getRuntime().exec(shutdownCmd);
	           } catch (IOException ex) {
	               Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
	           }
	   }
   public void logOff(){
	   String shutdownCmd = "shutdown -l";
       try {
    	   		Runtime.getRuntime().exec(shutdownCmd);
       } catch (IOException ex) {
           Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
   public void sleep(){
	   String shutdownCmd = "Rundll32.exe powrprof.dll,SetSuspendState Sleep";
	   try {
		   timeRemain();
		   Runtime.getRuntime().exec(shutdownCmd);
       } catch (IOException ex) {
           Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
     }
   }
   public void hibernate() {
	   String shutdownCmd = "shutdown -h";
	   try {
		   Runtime.getRuntime().exec(shutdownCmd);
       } catch (IOException ex) {
           Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
     }
		
	}
    public void cancel(){
    	String shutdownCmd = "shutdown -a";
           try {
        	   Runtime.getRuntime().exec(shutdownCmd);
           } catch (IOException ex) {
               Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE, null, ex);
         }
   }
    //วันที่ปัจจุบัน
    public static String currentTime(String format) {
    	try
    	{
    	DateFormat dateFormat = new SimpleDateFormat(format,Locale.US);
    	Date date = new Date(); 
    	return dateFormat.format(date).toString();
    	}
    	catch (Exception e)
    	{
    	e.printStackTrace();
    	return null;
    	}
    	}
    public void start(){
         t = Thread.currentThread();
        if(!t.isAlive()){
            t.start();
        }   
    }

   @Override
   public void run(){

           if (running && !t.isAlive()) {
               this.start();
           }
           running = false;
           t = null;
   }
   
    public static void main(String[] args){
          new AutoShutDown().run(); 
         // System.out.println(currentTime(format));
    }
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}

class syncMutex{
  static final Object mutex = new Object();
   static  int val = 0;
}
