package project.autoshutdown;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingUtilities;

public class AutoShutDownV2 extends AutoShutDownV1 {
	public boolean running;
	public static Object MUTEX;
	public int sec;
	public int count;
	static int a, b, c, d, hr, mn, sec2, seconds, rem;
	Thread t;
	static JLabel label;
	JPanel pp, aa, pd ,pd2;
	static int i;
	static Font font = new Font("Courier New", Font.BOLD, 40);
	static String hrStr, mnStr, secStr;
	protected JMenuBar menuBar;
	protected JMenu opt;
	protected JMenuItem setPass, hide, exit;
	protected static String password = "0000";
	protected static String whatTheUserEntered;
	protected String passInput = "" , text;
	protected JDialog d1;
	protected static JDialog d2;
	protected static JPasswordField pInput1 , pInput2 ;
	protected JLabel oldPass1;
	protected JLabel oldPass2;
	protected JLabel newPassLabel;
	protected JPasswordField newPass , oldInput1 , oldInput2;
	protected JButton okButton, cancelButton , changeB , cancelButton2;
	protected JLabel passLabel;
	protected JButton cancelButton3;
	
	
	public static void createAndShowGUI() {
		AutoShutDownV2 asd = new AutoShutDownV2();
		asd.createComponents();
		asd.setFrameFeatures();
		asd.addListeners();
	}

	private void addListeners() {
		startB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (sleep.isSelected()) {
					counter.start();

				}
				if (hibernate.isSelected()) {
					System.out.print("System is hibernating ");
					counter.start();

				}
				if (shutDown.isSelected()) {
					counter.start();
				}
				if (reStart.isSelected()) {
					System.out.print("System is restarting ");
					counter.start();
				}
				if (logOff.isSelected()) {
					System.out.print("System is loging ");
					counter.start();
				}
			}
		});

		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				passInput = pInput1.getText(); //Str ที่ได้จาก ช่องใส่พาส
				if (passInput.equals(text)) {
					
					d2.setVisible(false);
					
					pInput1.setText("");
					
					
				} else {
					JOptionPane.showMessageDialog(null, "Enter Wrong Password");
					d2.setVisible(true);
				}
			}
		});
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pd.add(cancelButton3);
				pd.remove(cancelButton);
				d2.setVisible(true);
				if (passInput.equals(text)) {
					System.exit(0);
				} else
					d2.setVisible(false);
			}
		});
		
		setPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				d1.setVisible(true);
			}
		});
		
		cancelButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				d1.setVisible(false);
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				System.exit(0);
			}
		});
		
		changeB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String oldPass1  = oldInput1.getText(); //Str ที่ได้จาก ช่องใส่พาส
				String oldPass2 = oldInput2.getText(); //Str ที่ได้จาก ช่องใส่พาส
				String pass = newPass.getText();
				if ((oldPass1.equals (oldPass2) && (oldPass1.equals( text)))){
					
					text = pass ;
					
					JOptionPane.showMessageDialog(null, "Passwod Have been Changed !");
					d1.setVisible(false);												
				}				
				else {JOptionPane.showMessageDialog(null, "Enter Worng Password / Password Does'nt Match");
				d1.setVisible(true);}
			}
		});

		exitB.addActionListener(new ButtonListener());
	}

	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == exitB) {
								pd.remove(cancelButton);
				d2.setVisible(true);
				pInput1.setText("");
				if (passInput.equals(text)) {
					System.exit(0);
				}
				else d2.setVisible(false);
			}
		}
	}

	// countdown
	public static Thread counter = new Thread() {
		public void run() {
			a = Integer.parseInt(hrTxt.getText());
			b = Integer.parseInt(minTxt.getText());
			c = (a * 3600) + (b * 60);
			d = c;

			seconds = c;
			hr = (int) (seconds / 3600);
			rem = (int) (seconds % 3600);
			mn = rem / 60;
			sec2 = rem % 60;

			for (i = c; i >= -1; i = i - 1) {
				d--;
				sec2--;
				if (i == 120) {
					Toolkit.getDefaultToolkit().beep();

					JOptionPane pane = new JOptionPane("SYSTEM WILL SHUTDOWN IN 2 MINUTES" , JOptionPane.WARNING_MESSAGE);
					JDialog dialog = pane.createDialog("WARNING" );
					dialog.setModal(false);
					dialog.setVisible(true);
					
				}
				
				if (i == 600) {
					Toolkit.getDefaultToolkit().beep();

					JOptionPane pane = new JOptionPane("SYSTEM WILL SHUTDOWN IN 10 MINUTES" , JOptionPane.WARNING_MESSAGE);
					JDialog dialog = pane.createDialog("WARNING" );
					dialog.setModal(false);
					dialog.setVisible(true);
					
				}
				
				if (i == 300) {
					Toolkit.getDefaultToolkit().beep();

					JOptionPane pane = new JOptionPane("SYSTEM WILL SHUTDOWN IN 5 MINUTES" , JOptionPane.WARNING_MESSAGE);
					JDialog dialog = pane.createDialog("WARNING" );
					dialog.setModal(false);
					dialog.setVisible(true);
					
				}
				if(sec2==-1){       
            		sec2=sec2+60;
            		mn--;         
            	}
            	if(mn==-1&&hr > 0){
            		hr--;
            		mn=mn+60;
            	} 

				// ตัวโปรแกรม
				if (d == 0 || d == -1) {
					if (sleep.isSelected()) {
						sleep();
					}
					if (hibernate.isSelected()) {
						hibernate();
					}
					if (shutDown.isSelected()) {
						shutDown();
					}
					if (reStart.isSelected()) {
						restart();
					}
					if (logOff.isSelected()) {
						logOff();
					}

				}
				updateGUI(i, label);
				try {
					Thread.sleep(1000);

				} catch (InterruptedException evnt) {};
			}
		}

		public void sleep() {
			String shutdownCmd = "Rundll32.exe powrprof.dll,SetSuspendState Sleep";
			try {
				Runtime.getRuntime().exec(shutdownCmd);
			} catch (IOException ex) {
				Logger.getLogger(AutoShutDown.class.getName()).log(
						Level.SEVERE, null, ex);
			}System.out.print("sleep2");
		}
	};

	// A method which updates GUI (sets a new value of JLabel).
	private static void updateGUI(final int i, final JLabel label) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				DecimalFormat df = new DecimalFormat("00");
				label.setText(df.format(hr) + " : " + df.format(mn) + " : "+ df.format(sec2));
				label.setFont(font);
				window.setTitle("Auto ShutDown "+df.format(hr) + " : " + df.format(mn) + " : "+ df.format(sec2));
			}
		});
	}

	// ควบคุมโปรแกรม
	public static void shutDown() {
		String shutdownCmd = "shutdown -s";
		try {

			Runtime.getRuntime().exec(shutdownCmd);
		} catch (IOException ex) {
			Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE,
					null, ex);
		}System.out.print("shut down");
	}

	public static void restart() {
		String shutdownCmd = "shutdown -r";
		try {
			Runtime.getRuntime().exec(shutdownCmd);
		} catch (IOException ex) {
			Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE,
					null, ex);
		}System.out.print("restart");
	}

	public static void logOff() {
		String shutdownCmd = "shutdown -l";
		try {
			Runtime.getRuntime().exec(shutdownCmd);
		} catch (IOException ex) {
			Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE,
					null, ex);
		}System.out.print("logoof");
	}

	public void sleep() {
		String shutdownCmd = "Rundll32.exe powrprof.dll,SetSuspendState Sleep";
		try {
			Runtime.getRuntime().exec(shutdownCmd);
		} catch (IOException ex) {
			Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE,
					null, ex);
		}System.out.print("sleep");
	}

	public static void hibernate() {
		String shutdownCmd = "shutdown -h";
		try {
			Runtime.getRuntime().exec(shutdownCmd);
		} catch (IOException ex) {
			Logger.getLogger(AutoShutDown.class.getName()).log(Level.SEVERE,
					null, ex);
		}System.out.print(" hibernate");

	}
	// วันที่ปัจจุบัน
	public static String currentTime(String format) {
		try {
			DateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
			Date date = new Date();
			return dateFormat.format(date).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void start() {
		t = Thread.currentThread();
		if (!t.isAlive()) {
			t.start();
		}
	}

	public void createComponents() {
		super.createComponents();
		cancelButton3 = new JButton("Cancel");
		pd = new JPanel();
		pInput1 = new JPasswordField("",20); // ช่องใส่พาสเวิร์ด
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		passLabel = new JLabel("Please enter current password (Defualt is 0000 )");
		text = "0000";

		pd.add(passLabel);
		pd.add(pInput1);
		pd.add(okButton);
		pd.add(cancelButton);
		
		pd2 = new JPanel();
		oldPass1 = new JLabel("Old - Password");
		oldPass2 = new JLabel("Confirm Old - Password");
		oldInput1 = new JPasswordField("" , 30);
		oldInput2 = new JPasswordField("" , 30);
		changeB = new JButton("Change Password");
		newPassLabel = new JLabel("New Password");
		newPass = new  JPasswordField("" , 15);
		cancelButton2 = new JButton("Cancel");
		
		
		pd2.add(oldPass1);	
		pd2.add(oldInput1);
		pd2.add(oldPass2);
		pd2.add(oldInput2);
		pd2.add(newPassLabel);
		pd2.add(newPass);
		pd2.add(changeB , BorderLayout.SOUTH);
		pd2.add(cancelButton2 , BorderLayout.SOUTH);
		
		ImageIcon icon = new ImageIcon("bin/images/icon.jpg");
		d1 = new JDialog();
		d1.setIconImage(icon.getImage());
		d1.add(pd2);
		d1.setVisible(false);
		d1.setSize(360, 220);
		d1.setModal(true);
		d1.setLocationRelativeTo(null);
		

		d2 = new JDialog();
		d2.setBackground(Color.lightGray);
		d2.setIconImage(icon.getImage());
		d2.add(pd);
		d2.setVisible(false);
		d2.setModal(true);
		d2.setSize(300, 130);
		d2.setLocationRelativeTo(null);
		d2.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

		 pp = new JPanel();
		 pp.setOpaque(false);
		 label = new JLabel("00:00:00");
		 pp.add(label,BorderLayout.EAST);
		 pp.setLocation(150,0);
		 label.setFont(font);
		
		 coutDownP.add(pp,BorderLayout.CENTER); 

		menuBar = new JMenuBar();
		opt = new JMenu("Option");
		setPass = new JMenuItem("Set Password");
		hide = new JMenuItem("Hide");
		exit = new JMenuItem("Exit");

		menuBar.add(opt);
		opt.add(setPass);
		opt.add(exit);
		window.setJMenuBar(menuBar);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				createAndShowGUI();
				d2.setVisible(true);
				

			}
		});
	}
}
